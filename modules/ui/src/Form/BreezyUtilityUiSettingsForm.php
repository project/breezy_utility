<?php

namespace Drupal\breezy_utility_ui\Form;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\breezy_utility\BreezyUtilityClassesManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Breezy Utility configuration form.
 */
class BreezyUtilityUiSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'breezy_utility.settings';

  /**
   * Drupal\breakpoint\BreakpointManagerInterface definition.
   *
   * @var \Drupal\breakpoint\BreakpointManagerInterface
   */
  protected BreakpointManagerInterface $breakpointManager;

  /**
   * BreezyUtilityClassesManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityClassesManagerInterface
   */
  protected BreezyUtilityClassesManagerInterface $utilityClassesManager;

  /**
   * Constructs a new settings form object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager
   *   The breakpoint manager service.
   * @param \Drupal\breezy_utility\BreezyUtilityClassesManagerInterface $breezy_utility_classes_manger
   *   The BreezyUtilityClassesManager.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typed_config_manager
   *   The typed config manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BreakpointManagerInterface $breakpoint_manager, BreezyUtilityClassesManagerInterface $breezy_utility_classes_manger, ?TypedConfigManager $typed_config_manager = NULL) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->breakpointManager = $breakpoint_manager;
    $this->utilityClassesManager = $breezy_utility_classes_manger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $container->get('config.factory');
    /** @var \Drupal\breakpoint\BreakpointManagerInterface $breakpoint_manager */
    $breakpoint_manager = $container->get('breakpoint.manager');
    /** @var \Drupal\breezy_utility\BreezyUtilityClassesManagerInterface $breezy_utility_classes_manager */
    $breezy_utility_classes_manager = $container->get('utility_classes.manager');
    return new static(
      $config_factory,
      $breakpoint_manager,
      $breezy_utility_classes_manager
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breezy_utility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $classes_groups = $this->utilityClassesManager->getGroups();
    $breakpoints_wrapper_id = 'breakpoints-wrapper';
    $breakpoint_group = $config->get('breakpoint_group');
    if ($form_state->getValue('breakpoint_group')) {
      $breakpoint_group = $form_state->getValue('breakpoint_group');
    }

    $form['utility_classes_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the utility class group.'),
      '#required' => TRUE,
      '#options' => $classes_groups,
      '#default_value' => $config->get('utility_classes_group'),
    ];

    $form['breakpoint_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Breakpoint group'),
      '#description' => $this->t('Select the breakpoints that should be included.'),
      '#default_value' => $breakpoint_group ?? '',
      '#options' => $this->breakpointManager->getGroups(),
      '#empty_option' => $this->t('- Select -'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'changeBreakpointGroup'],
        'wrapper' => $breakpoints_wrapper_id,
      ],
    ];

    $form['breakpoints'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#title' => $this->t('Breakpoints'),
      '#prefix' => '<div id="' . $breakpoints_wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    if ($breakpoint_group) {
      $breakpoint_group_breakpoints = $this->breakpointManager->getBreakpointsByGroup($breakpoint_group);

      $form['breakpoints']['#type'] = 'fieldset';

      foreach ($breakpoint_group_breakpoints as $breakpoint_name => $breakpoint) {
        $breakpoints = $form_state->getValue('breakpoints');
        if (!$breakpoints) {
          $breakpoints = $config->get('breakpoints');
        }

        $breakpoint_name = str_replace('.', '__', $breakpoint_name);
        $form['breakpoints'][$breakpoint_name] = [
          '#type' => 'fieldset',
          '#title' => $breakpoint->getLabel(),
        ];

        $form['breakpoints'][$breakpoint_name]['prefix'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Breakpoint prefix'),
          '#default_value' => $breakpoints[$breakpoint_name]['prefix'] ?? '',
        ];

      }
    }

    $form['cdn'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('CDN'),
      '#description' => $this->t('Load CSS library from a CDN'),
      '#tree' => TRUE,
    ];
    $cdn = $config->get('cdn');
    $form['cdn']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#default_value' => $cdn['enabled'] ?? FALSE,
    ];
    $form['cdn']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('Library CDN URL'),
      '#default_value' => $cdn['url'] ?? '',
      '#states' => [
        'visible' => [
          ':input[name="cdn[enabled]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="cdn[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $group = $form_state->getValue('utility_classes_group');
    $config->set('utility_classes_group', $group);
    $breakpoint_group = $form_state->getValue('breakpoint_group');
    $config->set('breakpoint_group', $breakpoint_group);
    $breakpoints = $form_state->getValue('breakpoints');
    $config->set('breakpoints', $breakpoints);
    $cdn = $form_state->getValue('cdn');
    $config->set('cdn', [
      'enabled' => $cdn['enabled'] ?? FALSE,
      'url' => $cdn['url'] ?? '',
    ]);

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for Breakpoint group selection.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The breakpoints portion of the form array.
   */
  public function changeBreakpointGroup(array &$form, FormStateInterface $form_state) {
    $breakpoint_group = $form_state->getValue(['plugin_configuration', 'breakpoint_group']);
    $form_state->set('breakpoint_group', $breakpoint_group);
    $form_state->setRebuild();
    return $form['breakpoints'];
  }

}
