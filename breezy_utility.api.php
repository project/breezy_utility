<?php

/**
 * @file
 * Hooks related to Breezy Utility module.
 */

// phpcs:disable DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the information about utility class plugins.
 *
 * @param array $definitions
 *   The array of utility class plugins.
 */
function hook_breezy_utility_classes_alter(array &$definitions) {

}
