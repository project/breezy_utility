<?php

namespace Drupal\breezy_utility\Form;

use Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface;
use Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface;
use Drupal\breezy_utility\Utility\BreezyUtilityElementHelper;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base class for deleting.
 */
abstract class BreezyUtilityDeleteFormBase extends ConfirmFormBase {

  use BreezyUtilityDialogFormTrait;

  /**
   * BreezyUtilityElementInterface array.
   *
   * @var array
   */
  protected array $element;

  /**
   * BreezyUtilityElementPluginManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityElementPluginManagerInterface
   */
  protected BreezyUtilityElementPluginManagerInterface $elementManager;

  /**
   * {@inheritdoc}
   */
  public function getFormName() {
    return 'breezy_utility_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'confirmation';
    $form['#theme'] = 'confirm_form';
    $form[$this->getFormName()] = ['#type' => 'hidden', '#value' => 1];
    $form['#title'] = $this->getQuestion();
    $form['description'] = $this->getDescription();

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Confirm'),
    ];
    return $this->buildDialogForm($form, $form_state);
  }

  /**
   * Return the element plugin associated with this form.
   *
   * @return \Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface
   *   An element.
   *
   * @throws \Exception
   */
  protected function getElementPlugin(): BreezyUtilityElementInterface {
    return $this->elementManager->getElementInstance((array) $this->element);
  }

  /**
   * Get the element title from the element.
   *
   * @return string
   *   The element title.
   *
   * @throws \Exception
   */
  protected function getElementTitle(): string {
    $element = $this->getElementPlugin();
    return BreezyUtilityElementHelper::getElementTitle($element);
  }

}
