<?php

namespace Drupal\breezy_utility\Ajax;

use Drupal\Core\Ajax\CloseDialogCommand;

/**
 * Provides an Ajax command to close Breezy dialog and tray.
 *
 * This command is implemented in
 * Drupal.AjaxCommands.prototype.breezyCloseDialog.
 */
class BreezyUtilityCloseDialogCommand extends CloseDialogCommand {

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'breezyCloseDialog',
      'selector' => $this->selector,
      'persist' => $this->persist,
    ];
  }

}
