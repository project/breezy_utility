<?php

namespace Drupal\breezy_utility\Ajax;

use Drupal\Core\Ajax\RedirectCommand;

/**
 * Provides an Ajax command for refreshing Breezy forms.
 *
 * This command is implemented in
 * Drupal.AjaxCommands.prototype.breezyRefresh.
 */
class BreezyUtilityRefreshCommand extends RedirectCommand {

  /**
   * Implements \Drupal\Core\Ajax\CommandInterface::render().
   */
  public function render() {
    return [
      'command' => 'breezyRefresh',
      'url' => $this->url,
    ];
  }

}
