<?php

namespace Drupal\breezy_utility\Ajax;

use Drupal\Core\Ajax\HtmlCommand;

/**
 * Provides an Ajax command for calling the jQuery html() method.
 *
 * This command is implemented in
 * Drupal.AjaxCommands.prototype.breezyInsert.
 */
class BreezyUtilityHtmlCommand extends HtmlCommand {

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'breezyInsert',
      'method' => 'html',
      'selector' => $this->selector,
      'data' => $this->getRenderedContent(),
      'settings' => $this->settings,
    ];
  }

}
