<?php

namespace Drupal\breezy_utility;

/**
 * Interface for UtilityClass plugins.
 */
interface UtilityClassInterface {

  /**
   * Returns the translated label.
   *
   * @return string
   *   The translated label.
   */
  public function getLabel();

  /**
   * Returns the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId();

  /**
   * A URL to the property API documentation.
   *
   * @return \Drupal\Core\Url|null
   *   The property documentation url.
   */
  public function getPropertyApiUrl();

  /**
   * A link to the property API documentation.
   *
   * @return \Drupal\Core\GeneratedLink|string
   *   A link to property's API documentation.
   */
  public function getPropertyApiLink();

  /**
   * Returns the CSS property.
   *
   * @return string
   *   The css property.
   */
  public function getCssProperty();

  /**
   * Returns the Classes.
   *
   * @return array
   *   The array of classes.
   */
  public function getClasses();

  /**
   * Returns the provider.
   *
   * @return string
   *   The provider.
   */
  public function getProvider();

  /**
   * Returns the breakpoint group.
   *
   * @return string
   *   The breakpoint group.
   */
  public function getGroup();

}
