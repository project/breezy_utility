<?php

namespace Drupal\breezy_utility;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Provides an interface for UtilityClasses plugin managers.
 */
interface BreezyUtilityClassesManagerInterface extends PluginManagerInterface {

  /**
   * Gets utility classes for the group.
   *
   * @param string $group
   *   The utility classes group.
   *
   * @return \Drupal\breezy_utility\UtilityClassInterface[]
   *   Array of utility classes keyed by machine name.
   */
  public function getUtilityClassesByGroup(string $group);

  /**
   * Gets all the existing utility classes groups.
   *
   * @return array
   *   Array of utility classes group labels, keyed by group name.
   */
  public function getGroups();

  /**
   * Gets all the providers for the specified utility classes group.
   *
   * @param string $group
   *   The utility classes group to retrieve.
   *
   * @return array
   *   An array keyed by the provider name with values of provider type (module
   *   or theme).
   */
  public function getGroupProviders(string $group);

}
