<?php

namespace Drupal\breezy_utility\Utility;

use Drupal\Component\Serialization\Json;

/**
 * Helper class for modal and off-canvas dialog methods.
 */
class BreezyUtilityDialogHelper {

  /**
   * Width for wide dialog. (modal: 1000px; off-canvas: 800px)
   *
   * Used by: Video only.
   *
   * @var string
   */
  const DIALOG_WIDE = 'wide';

  /**
   * Width for normal dialog. (modal: 800px; off-canvas: 600px)
   *
   * Used by: Add and edit element/handler, etc…
   *
   * @var string
   */
  const DIALOG_NORMAL = 'normal';

  /**
   * Width for narrow dialog. (modal: 700px; off-canvas: 500px)
   *
   * Used by: Duplicate and delete entity, notes, etc…
   *
   * @var string
   */
  const DIALOG_NARROW = 'narrow';

  /**
   * Prevent dialog from being displayed.
   *
   * @var string
   */
  const DIALOG_NONE = 'none';

  /**
   * Get modal dialog attributes.
   *
   * @param int|string $width
   *   Width of the off-canvas dialog.
   * @param array $class
   *   Additional class names to be included in the dialog attributes.
   *
   * @return array
   *   An array of modal dialog attributes.
   */
  public static function getModalDialogAttributes($width = self::DIALOG_NORMAL, array $class = []) : array {
    $dialog_widths = [
      static::DIALOG_WIDE => 1000,
      static::DIALOG_NORMAL => 800,
      static::DIALOG_NARROW => 700,
    ];
    $width = $dialog_widths[$width] ?? $width;
    $class[] = 'breezy-ajax-link';
    return [
      'class' => $class,
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode([
        'width' => $width,
        'dialogClass' => 'breezy-ui-dialog',
      ]),
    ];
  }

  /**
   * Get off-canvas dialog attributes.
   *
   * @param int|string $width
   *   Width of the off-canvas dialog.
   * @param array $class
   *   Additional class names to be included in the dialog attributes.
   *
   * @return array
   *   Modal dialog attributes.
   */
  public static function getOffCanvasDialogAttributes($width = self::DIALOG_NORMAL, array $class = []): array {

    if ($width === self::DIALOG_NONE) {
      return $class ? ['class' => $class] : [];
    }

    $dialog_widths = [
      static::DIALOG_WIDE => 800,
      static::DIALOG_NORMAL => 600,
      static::DIALOG_NARROW => 400,
    ];
    $width = $dialog_widths[$width] ?? $width;

    $class[] = 'breezy-ajax-link';
    return [
      'class' => $class,
      'data-dialog-type' => 'dialog',
      'data-dialog-renderer' => 'off_canvas',
      'data-dialog-options' => Json::encode([
        'width' => $width,
        'dialogClass' => 'ui-dialog-off-canvas breezy-off-canvas',
      ]),
    ];
  }

}
