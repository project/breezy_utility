<?php

namespace Drupal\breezy_utility\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textfield;

/**
 * Provides a BreezyUtilityPropertyAutocomplete element.
 *
 * @FormElement("breezy_utility_property_autocomplete")
 */
class BreezyUtilityPropertyAutocomplete extends Textfield {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    $info = parent::getInfo();
    $info['#autocomplete_route_name'] = 'breezy_utility.property_controller';
    $info['#element_validate'][] = [$class, 'validateAutocomplete'];
    return $info;
  }

  /**
   * Validate autocomplete.
   */
  public static function validateAutocomplete(array $element, FormStateInterface $form_state, $form) {
    $value = $form_state->getValue($element['#name']);
    $classService = \Drupal::service('breezy_utility.utility_classes');
    $classes = $classService->getClassOptions($value);
    if (!$classes) {
      $form_state->setError($element, 'Element did not validate.');
    }

  }

}
