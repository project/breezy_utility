<?php

namespace Drupal\breezy_utility\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a BreezyUtilityClassSelect element.
 *
 * @FormElement("breezy_utility_class_select")
 */
class BreezyUtilityClassSelect extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#property' => NULL,
      '#multiple' => FALSE,
      '#required' => FALSE,
      '#title_display' => 'invisible',
      '#process' => [
        [$class, 'processBreezyUtilityClassSelect'],
        [$class, 'processAjaxForm'],
      ],
      '#pre_render' => [
        [$class, 'preRenderBreezyUtilityClassSelect'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input === FALSE) {
      if (isset($element['#default_value'])) {
        $default_value = $element['#default_value'];
        return $default_value;
      }
      else {
        return '';
      }
    }
    elseif (!empty($input['value'])) {
      return $input['value'];
    }
    else {
      return '';
    }
  }

  /**
   * Process a breezy_utility_class_select element.
   *
   * @param array $element
   *   The element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public static function processBreezyUtilityClassSelect(array &$element, FormStateInterface $form_state) {

    if (isset($element['#property'])) {

      $property = $element['#property'];
      /** @var \Drupal\breezy_utility\BreezyUtilityClassServiceInterface $class_service */
      $class_service = \Drupal::service('breezy_utility.utility_classes');
      $options = $class_service->getClassOptions($property);

      $default_value = $element['#default_value'] ?? '';

      $element['value'] = [
        '#type' => 'select',
        '#multiple' => FALSE,
        '#empty_option' => t('-- Select --'),
        '#options' => $options,
        '#required' => $element['required'] ?? FALSE,
        '#default_value' => $default_value,
        '#property' => $property,
      ];

    }

    // Add validate callback.
    $element += ['#element_validate' => []];
    array_unshift($element['#element_validate'], [get_called_class(), 'validateBreezyUtilityClassSelectValue']);

    return $element;
  }

  /**
   * Prepares a BreezyUtilityClassSelect element.
   *
   * @param array $element
   *   The element array.
   *
   * @return array
   *   The element array.
   */
  public static function preRenderBreezyUtilityClassSelect(array $element) : array {
    Element::setAttributes($element, [
      '#id' => 'id',
      '#name' => 'breezy',
      '#size' => 'size',
    ]);
    static::setAttributes($element, ['form-select']);
    return $element;
  }

  /**
   * Validates a BreezyUtilityClassSelect element.
   *
   * @param array $element
   *   The element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $complete_form
   *   The complete form array.
   */
  public static function validateBreezyUtilityClassSelectValue(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    if (empty($value['value'])) {
      // @todo Add error.
    }

    $form_state->setValueForElement($element['value'], NULL);

    $element['#value'] = $value['value'];
    $form_state->setValueForElement($element, $value['value']);
  }

}
