<?php

namespace Drupal\breezy_utility;

use Drupal\Core\Url;

/**
 * Provides an interface for BreezyUtilityClassService.
 */
interface BreezyUtilityClassServiceInterface {

  /**
   * Get class options.
   *
   * @param string $css_property
   *   The utility_class css property.
   *
   * @return array
   *   An array of class options.
   */
  public function getClassOptions(string $css_property) : array;

  /**
   * Get property options.
   *
   * @return array
   *   An array of property options, keyed by property.
   */
  public function getPropertyOptions() : array;

  /**
   * Get API URL by CSS property.
   *
   * @param string $css_property
   *   The CSS property name.
   *
   * @return \Drupal\Core\Url|null
   *   The property API documentation url.
   */
  public function getPropertyApiUrl(string $css_property): Url|NULL;

}
