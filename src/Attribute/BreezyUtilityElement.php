<?php

declare(strict_types=1);

namespace Drupal\breezy_utility\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a new BreezyUtilityElement plugin attribute.
 *
 * Plugin Namespace: Plugin\BreezyUtility\BreezyUtilityElement.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class BreezyUtilityElement extends Plugin {

  /**
   * Constructs a new BreezyUtilityElement attribute.
   *
   * @param string $id
   *   The plugin id.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $label
   *   The human-readable name of the element.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the element.
   * @param bool $hidden
   *   The element is hidden from configuration forms.
   * @param bool $multiple
   *   The element allows multiple values.
   * @param bool $container
   *   The element is a 'container'.
   * @param bool $ui
   *   The element has a UI.
   */
  public function __construct(
    public readonly string $id,
    public readonly TranslatableMarkup $label,
    public readonly ?TranslatableMarkup $description,
    public readonly bool $hidden = FALSE,
    public readonly bool $multiple = FALSE,
    public readonly bool $container = FALSE,
    public readonly bool $ui = FALSE,
  ) {}

}
