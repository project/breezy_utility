<?php

namespace Drupal\breezy_utility;

use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;

/**
 * Default object used for utility class plugins.
 *
 * @see \Drupal\breezy_utility\BreezyUtilityClassesManager
 * @see plugin_api
 */
class UtilityClass extends PluginBase implements UtilityClassInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->t('@label', ['@label' => $this->pluginDefinition['label']], ['context' => 'utility_classes']);
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyApiUrl() {
    return (!empty($this->pluginDefinition['api'])) ? Url::fromUri($this->pluginDefinition['api']) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyApiLink() {
    $api_url = $this->getPropertyApiUrl();
    return ($api_url) ? Link::fromTextAndUrl($this->getLabel(), $api_url) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return $this->pluginDefinition['provider'];
  }

  /**
   * {@inheritdoc}
   */
  public function getGroup() {
    return $this->pluginDefinition['group'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssProperty() {
    return $this->pluginDefinition['css_property'];
  }

  /**
   * {@inheritdoc}
   */
  public function getClasses() {
    return $this->pluginDefinition['classes'];
  }

}
