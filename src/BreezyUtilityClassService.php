<?php

namespace Drupal\breezy_utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;

/**
 * Provides a service for working with utility class plugins.
 */
class BreezyUtilityClassService implements BreezyUtilityClassServiceInterface {

  /**
   * Drupal\breezy_utility\BreezyUtilityClassesManagerInterface definition.
   *
   * @var \Drupal\breezy_utility\BreezyUtilityClassesManagerInterface
   */
  protected $classesManager;

  /**
   * Drupal\Core\Config\ImmutableConfig definition.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Utility "group" settings key.
   *
   * @var string
   */
  const GROUP_KEY = 'utility_classes_group';

  /**
   * Constructs a new BreezyUtilityClassService.
   *
   * @param \Drupal\breezy_utility\BreezyUtilityClassesManagerInterface $classes_manager
   *   The classes plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The immutable config.
   */
  public function __construct(BreezyUtilityClassesManagerInterface $classes_manager, ConfigFactoryInterface $config) {
    $this->classesManager = $classes_manager;
    $this->config = $config->get('breezy_utility.settings');
  }

  /**
   * Get classes for property.
   *
   * @param string $css_property
   *   The utility_class css property.
   *
   * @return array
   *   The utility classes.
   */
  public function getUtilityClasses(string $css_property) {
    $classes = [];
    $utility_classes = $this->getAllUtilityClasses();
    foreach ($utility_classes as $utility_class) {
      if ($utility_class->getCssProperty() == $css_property) {
        $classes = $utility_class->getClasses();
      }
    }
    return $classes;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyOptions() : array {
    $options = [];
    $utility_classes = $this->getAllUtilityClasses();
    foreach ($utility_classes as $utility_class) {
      $options[$utility_class->getCssProperty()] = $utility_class->getLabel();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getClassOptions(string $css_property) : array {
    $options = [];
    $utility_classes = $this->getAllUtilityClasses();
    foreach ($utility_classes as $utility_class) {
      if ($utility_class->getCssProperty() == $css_property) {
        $classes = $utility_class->getClasses();
        foreach ($classes as $class) {
          $options[$class] = $class;
        }
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyApiUrl(string $css_property): Url|NULL {

    $utility_classes = $this->getAllUtilityClasses();
    foreach ($utility_classes as $utility_class) {
      if ($utility_class->getCssProperty() == $css_property) {
        return $utility_class->getPropertyApiUrl();
      }
    }
    return NULL;
  }

  /**
   * Get all utility classes.
   *
   * @return \Drupal\breezy_utility\UtilityClassInterface[]
   *   An array of utility classes.
   */
  protected function getAllUtilityClasses() : array {
    $group = $this->config->get(static::GROUP_KEY);
    return $this->classesManager->getUtilityClassesByGroup($group);
  }

}
