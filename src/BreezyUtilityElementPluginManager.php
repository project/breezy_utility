<?php

namespace Drupal\breezy_utility;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a manager for Breezy Layouts Element plugins.
 */
class BreezyUtilityElementPluginManager extends DefaultPluginManager implements BreezyUtilityElementPluginManagerInterface {

  use DependencySerializationTrait;

  /**
   * Constructs a new BreezyLayoutsVariantPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/BreezyUtility/Element', $namespaces, $module_handler, 'Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface', BreezyUtilityElement::class);
    $this->setCacheBackend($cache_backend, 'breezy_utility_element_plugins');
    $this->alterInfo('breezy_utility_element');
  }

  /**
   * {@inheritdoc}
   */
  public function initializeElement(array &$element) {
    $element_plugin = $this->getElementInstance($element);
    $element_plugin->initialize($element);
  }

  /**
   * {@inheritdoc}
   */
  public function buildElement(array &$element, array $form, FormStateInterface $form_state) {

    $element_plugin = $this->getElementInstance($element);
    $element_plugin->prepare($element);
    $element_plugin->setDefaultValue($element);

    // Allow modules to alter the BreezyUtility element.
    // @see \Drupal\Core\Field\WidgetBase::formSingleElement()
    $hooks = ['breezy_utility_element'];
    if (!empty($element['#type'])) {
      $hooks[] = 'breezy_utility_element_' . $element['#type'];
    }
    $context = ['form' => $form];
    $this->moduleHandler->alter($hooks, $element, $form_state, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function getValidDefinitions($container = FALSE) : array {
    $definitions = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      if ($definition['container'] == $container) {
        $definitions[$id] = $definition;
      }
    }
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getElementPluginId(array $element): string {
    if (isset($element['#breezy_utility_plugin_id']) && $this->hasDefinition($element['#breezy_utility_plugin_id'])) {
      return $element['#breezy_utility_plugin_id'];
    }
    elseif (isset($element['#type']) && $this->hasDefinition($element['#type'])) {
      return $element['#type'];
    }
    elseif (isset($element['element']['type']) && $this->hasDefinition($element['element']['type'])) {
      return $element['element']['type'];
    }

    return $this->getFallbackPluginId(NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []): string {
    return 'breezy_utility_element';
  }

  /**
   * {@inheritdoc}
   */
  public function getElementInstance(array $element): BreezyUtilityElementInterface {
    $plugin_id = $this->getElementPluginId($element);

    /** @var \Drupal\breezy_utility\Plugin\BreezyUtility\Element\BreezyUtilityElementInterface $element_plugin */
    $element_plugin = $this->createInstance($plugin_id);

    return $element_plugin;
  }

}
