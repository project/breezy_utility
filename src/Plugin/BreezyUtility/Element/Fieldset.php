<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a fieldset element.
 */
#[BreezyUtilityElement(
  id: "fieldset",
  label: new TranslatableMarkup("Fieldset"),
  description: new TranslatableMarkup("Provides a fieldset element."),
  hidden: TRUE,
  multiple: FALSE,
)]
class Fieldset extends ContainerBase {

}
