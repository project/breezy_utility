<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a checkbox element.
 */
#[BreezyUtilityElement(
  id: "checkbox",
  label: new TranslatableMarkup("Checkbox"),
  description: new TranslatableMarkup("Provides a checkbox element."),
  hidden: TRUE,
  ui: TRUE,
)]
class Checkbox extends BreezyUtilityElementBase {

}
