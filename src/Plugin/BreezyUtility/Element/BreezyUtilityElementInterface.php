<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides an interface for BreezyUtilityElement plugins.
 */
interface BreezyUtilityElementInterface extends ConfigurableInterface, ContainerFactoryPluginInterface, PluginFormInterface {

  /**
   * Retrieves the plugin's label.
   *
   * @return string
   *   The plugin's human-readable and translated label.
   */
  public function label(): string;

  /**
   * Retrieves the plugin's description.
   *
   * @return string|null
   *   The plugin's translated description; or NULL if it has none.
   */
  public function getDescription();

  /**
   * Get default properties.
   *
   * @return array
   *   An associative array containing default element properties.
   */
  public function getDefaultProperties(): array;

  /**
   * Checks if the element carries a value.
   *
   * @param array $element
   *   An element.
   *
   * @return bool
   *   TRUE if the element carries a value.
   */
  public function isInput(array $element): bool;

  /**
   * Get an element's default property value.
   *
   * @param string $property_name
   *   An element's property name.
   *
   * @return mixed
   *   An element's default property value or NULL if default property does not
   *   exist.
   */
  public function getDefaultProperty(string $property_name);

  /**
   * Get an element's property value.
   *
   * @param array $element
   *   An element.
   * @param string $property_name
   *   An element's property name.
   *
   * @return mixed
   *   An element's property value, default value, or NULL if
   *   property does not exist.
   */
  public function getElementProperty(array $element, string $property_name);

  /**
   * Set an element's default value using saved data.
   *
   * The method allows the Variant's 'saved' #default_value to be different
   * from the element's #default_value.
   *
   * @param array $element
   *   An element.
   *
   * @see \Drupal\webform\Plugin\WebformElement\DateBase::setDefaultValue
   * @see \Drupal\webform\Plugin\WebformElement\EntityAutocomplete::setDefaultValue
   */
  public function setDefaultValue(array &$element);

  /**
   * Determine if the element supports a specified property.
   *
   * @param string $property_name
   *   An element's property name.
   *
   * @return bool
   *   TRUE if the element supports a specified property.
   */
  public function hasProperty($property_name): bool;

  /**
   * Checks if the element is hidden.
   *
   * @return bool
   *   TRUE if the element is hidden.
   */
  public function isHidden(): bool;

  /**
   * Checks if the element value has multiple values.
   *
   * @param array $element
   *   An element.
   *
   * @return bool
   *   TRUE if the element value has multiple values.
   */
  public function hasMultipleValues(array $element): bool;

  /**
   * If the form has a visible form widget.
   *
   * Used to show / hide form containers.
   *
   * @return bool
   *   TRUE if the element has a visible widget.
   */
  public function hasUi(): bool;

  /**
   * Provides a form to configure the element.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The form.
   */
  public function form(array $form, FormStateInterface $form_state): array;

}
