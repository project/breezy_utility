<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a textfield element which is only used in config forms.
 */
#[BreezyUtilityElement(
  id: 'textfield',
  label: new TranslatableMarkup("Text field"),
  description: new TranslatableMarkup("Provides a textfield form element."),
  hidden: TRUE,
  multiple: FALSE,
)]
class Textfield extends BreezyUtilityElementBase implements BreezyUtilityElementInterface {

}
