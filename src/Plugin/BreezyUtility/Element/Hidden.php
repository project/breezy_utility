<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a hidden element.
 */
#[BreezyUtilityElement(
  id: "hidden",
  label: new TranslatableMarkup("Hidden"),
  description: new TranslatableMarkup("Provides a hidden element."),
  hidden: FALSE,
  multiple: FALSE,
  ui: FALSE,
)]
class Hidden extends BreezyUtilityElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties(): array {
    return [
      'title' => '',
      'default_value' => '',
      'property' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    if (isset($form['element']['required'])) {
      unset($form['element']['required']);
    }

    if (is_array($form['#parents'])) {
      $parents = array_merge($form['#parents'], ['element', 'default_value']);
    }
    else {
      $parents = [$form['#parents'], 'element', 'default_value'];
    }

    $form['element']['default_value'] = [
      '#type' => 'breezy_utility_class_select',
      '#title' => $this->t('Property value'),
      '#property' => $form_state->get('property'),
      '#parents' => array_values($parents),
    ];

    return $form;
  }

}
