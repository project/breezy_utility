<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a Radios element.
 */
#[BreezyUtilityElement(
  id: 'radios',
  label: new TranslatableMarkup("Radios"),
  description: new TranslatableMarkup("Provides radios element."),
  hidden: FALSE,
  multiple: TRUE,
  container: FALSE,
  ui: TRUE,
)]
class Radios extends OptionsBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties(): array {
    $properties = [
      // Form display.
      'options_description_display' => 'description',
      'options__properties' => [],
      // Wrapper.
      'wrapper_type' => 'fieldset',
    ] + parent::defineDefaultProperties();
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function setDefaultValue(array &$element) {
    // Unset empty string as default option to prevent '' === '0' issue.
    // @see \Drupal\Core\Render\Element\Radio::preRenderRadio
    if (isset($element['#default_value'])
      && $element['#default_value'] === ''
      && !isset($element['#options'][$element['#default_value']])) {
      unset($element['#default_value']);
    }
  }

}
