<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a select element.
 */
#[BreezyUtilityElement(
  id: 'select',
  label: new TranslatableMarkup('Select'),
  description: new TranslatableMarkup('Provides a select element.'),
  hidden: FALSE,
  multiple: TRUE,
  container: FALSE,
  ui: TRUE,
)]
class Select extends OptionsBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties(): array {
    $properties = [
      'multiple' => FALSE,
      'empty_option' => '',
      'empty_value' => '',
    ] + parent::defineDefaultProperties();
    return $properties;
  }

}
