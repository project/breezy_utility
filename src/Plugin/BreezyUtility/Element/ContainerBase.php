<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

/**
 * Provides a base "container" class.
 */
abstract class ContainerBase extends BreezyUtilityElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties(): array {
    return [
      'title' => '',
      // Form validation.
      'required' => FALSE,
      // Attributes.
      'attributes' => [],
    ] + $this->defineDefaultBaseProperties();
  }

  /**
   * {@inheritdoc}
   */
  public function isInput(array $element): bool {
    return FALSE;
  }

}
