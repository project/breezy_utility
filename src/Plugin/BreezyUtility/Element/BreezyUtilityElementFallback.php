<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a fallback element.
 */
#[BreezyUtilityElement(
  id: 'breezy_utility_element',
  label: new TranslatableMarkup('Breezy utility element'),
  description: new TranslatableMarkup('Fallback element'),
  hidden: TRUE,
  container: FALSE,
  ui: FALSE,
)]
class BreezyUtilityElementFallback extends BreezyUtilityElementBase {

}
