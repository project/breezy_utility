<?php

namespace Drupal\breezy_utility\Plugin\BreezyUtility\Element;

use Drupal\breezy_utility\Attribute\BreezyUtilityElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a checkboxes element plugin.
 */
#[BreezyUtilityElement(
  id: 'checkboxes',
  label: new TranslatableMarkup('Checkboxes'),
  description: new TranslatableMarkup('Provides checkboxes element.'),
  hidden: FALSE,
  multiple: TRUE,
  container: FALSE,
  ui: TRUE,
)]
class Checkboxes extends OptionsBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties(): array {
    $properties = [
      'multiple' => TRUE,
      'multiple_error' => '',
      'options_description_display' => 'description',
      'options__properties' => [],
      'wrapper_type' => 'fieldset',
    ] + parent::defineDefaultProperties();
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function supportsMultipleValues(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasMultipleValues(array $element): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    // Checkboxes require > 2 options.
    $form['element']['multiple']['#min'] = 2;

    return $form;
  }

}
