# Breezy Utility

## Purpose

This is a utility module for integrating utility classes for other modules.


**Modules that depend on Breezy Utility**

[Breezy Layouts](https://www.drupal.org/project/breezy_layouts)

## What it does

The module implements a plugin type for utility classes.  Each plugin represents a single CSS property, and exposes all
the utility classes associated with that property.

## Why?

Other modules can leverage the plugins to expose the classes as needed.

Breezy Layouts uses the plugins for creating layout variants.

## How?

Plugins are generated from YAML files, and grouped by a utility class "group".

It ships with a subset of TailwindCSS classes.

>**NOTE** It also ships with a default set of Layout plugins.

Developers can extend and alter existing utility classes, or provide their own utility classes.

Utility class plugins are defined in a YAML file, `my_module.utility_classes.yml`.  Utility classes are associated with
a CSS property, like `margin`, `width`, and `display`.  Breezy Utility ships with a subset of TailwindCSS utility classes.

To see the utility classes Breezy Utility ships with, visit [breezy_utility.utility_classes.yml](https://git.drupalcode.org/project/breezy_utility/-/blob/1.0.x/breezy_utility.utility_classes.yml?ref_type=heads).

Utility classes are defined with:
```
module.group.id:
  id: #string
  group: #string
  css_property: #string
  classes: #array
```
For example, the `flex-wrap` CSS property:
```
breezy_utility.tailwind.flex_wrap:
  id: flex_wrap
  label: 'Flex wrap'
  group: breezy_utility.tailwind
  css_property: 'flex-wrap'
  classes: ["flex-wrap","flex-wrap-reverse","flex-nowrap",]
```
## How to use utility classes.

Breezy Utility provides a service, `breezy_utility.utility_classes`, which has a couple of helper methods for working
with utility class plugins.

Breezy Utility provides an autocomplete element for searching CSS properties, `breezy_utility_property_autocomplete`.  To
 use in a custom form, create a form field like:

```
$form['some_field'] = [
  '#type' => 'breezy_utility_property_autocomplete',
  '#title' => t('Search CSS properties'),
];
```

Also, Breezy Utility provides an element for selecting utility classes, `breezy_utility_class_select`.

```
$form['some_field'] = [
  '#type' => 'breezy_utility_class_select,
  '#property' => $css_property,
]
```

## How to extend utility classes.

For example, Breezy Utility does not (current) provide utility classes for CSS [float](https://tailwindcss.com/docs/float) property.

If you wanted to provide `float`, create a YAML file in a custom module or theme `*.utility_classes.yml`, then
add an entry like:

```
breezy_utility.tailwind.float:
  id: float
  label: 'Float'
  group: breezy_utility.tailwind
  css_property: 'float'
  classes: ["float-start", "float-end", "float-right", "float-left", "float-none",]
```

After a cache rebuild, floats should now be available.

## How to alter existing utility classes.

Identify the utility class group and ID of the CSS property you wish to alter.

Use `hook_breezy_utility_classes_alter` to alter Utility Classes.

For example, if you would like to add the CSS class `m-100` to the CSS property `margin`, you could create a function like:

```
function my_module_breezy_utility_classes_alter(array &$definitions) {
  foreach($definitions as $utility_class => $utility_class_properties) {
    if ($utility_class == 'breezy_utility.margin') {
      $utility_class_properties['classes'][] = 'm-100';
    }
  }
}
```

## How to use your own utility class system.

In your `mymodule.utility_classes.yml`, you can define your own utility class system.  Utility class "systems" are just
a bunch of utility class plugins that use the same `group` key.

Once you've designed your utility class plugins and enabled `breezy_utility_ui`, you can visit
/admin/config/content/breezy/settings and select your new system.
